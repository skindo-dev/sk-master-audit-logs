"use strict"

var masterLogger = function () {
    let crudder = null,
        nonTrackingFields = [],
        logger = null,
        _ = require("lodash"),
        cuti = require("cuti"),
        http = require("http"),
        entityType = "",
        options = {};

    this.init = function (_crudder, _logger, _options, _entityType) {
        crudder = _crudder;
        options = _options;
        nonTrackingFields = _options.nonTrackingFields;
        if (options.mergeCommentsWithStatus) {
            if (!options.statusCommentKeys) {
                options.statusCommentKeys = ["makerComments", "checkerComments"];
            }
            nonTrackingFields = nonTrackingFields.concat(options.statusCommentKeys)
        }
        logger = _logger;
        entityType = _entityType;
    }

    /**
    * Prehook for saving auditLogs in MasterAuditLogs
    * @param {*} next 
    */
    this.auditLog = function (next) {
        var self = this;
        crudder.model.findOne({ _id: self._id }).lean()
            .exec(function (err, doc) {
                if (err) {
                    logger.error("Error Ocuured " + err);
                    next();
                } else if (doc) {
                    var diff = compare(doc, self.toObject());
                    if (self.auditLogList == undefined) {
                        self.auditLogList = [];
                    }
                    var arrObj = buildAuditLogObjs(diff, doc, self.toObject(), nonTrackingFields);
                    if (arrObj.length == 0) {
                        var tempOne = {
                            createdBy: self.__metadata_username__ ? self.__metadata_username__ : self.modifiedBy,
                            createdAt: self.createdAt,
                            audit: []
                        }
                        tempOne.audit.push({
                            field: "_id",
                            oldStatus: "",
                            newStatus: self._id
                        })
                        self.auditLogList.push(tempOne);
                    }

                    else if (arrObj.length) {
                        var temp = {};
                        for (var i = 0; i < arrObj.length; i++) {
                            if (i == 0) {
                                temp.createdBy = arrObj[i].createdBy;
                                temp.createdAt = arrObj[i].createdAt;
                                temp.audit = [];
                            }
                            temp.audit.push({
                                "field": arrObj[i].field,
                                "oldStatus": arrObj[i].oldValue,
                                "newStatus": arrObj[i].newValue,
                            })
                        }
                        self.auditLogList.push(temp);
                    }
                    if (_.isEmpty(arrObj)) {
                        next();
                    } else {
                        //  self.auditLogList.push(temp);
                        saveAuditLogs(arrObj).then(savedAudit => {
                            next();
                        }).catch(err => {
                            logger.error(err);
                            next();
                        });
                    }
                } else {
                    next();
                }
            });
    }

    this.auditLogForUpdateHook = function (next) {
        var self = this._update.$set?this._update.$set:this._update?this._update:{};
        let select = Object.keys(self).map(x => x.replace(/\d\.+/g, '')).join(" ");
        crudder.model.findOne(this._conditions).select(select).lean()
            .exec(function (err, doc) {
                if (err) {
                    logger.error("Error Ocuured " + err);
                    next();
                }
                else if (doc) {
                    //workaround to keep id, as findOne returns id always. 
                    self._id = doc._id;
                    var diff = compare(doc, self);
                    var arrObj = buildAuditLogObjs(diff, doc, self, nonTrackingFields);
                    if (_.isEmpty(arrObj)) {
                        next();
                    } else {
                        //  self.auditLogList.push(temp);
                        saveAuditLogs(arrObj).then(savedAudit => {
                            next();
                        }).catch(err => {
                            logger.error(err);
                            next();
                        });
                    }
                } else {
                    next();
                }
            });
    }

    /**
    * This function is used to save the audit Logs in masterAuditLog collection
    * @param {*array of auditLogs} docs 
    */
    var saveAuditLogs = function (docs) {
        return new Promise((resolve, reject) => {
            cuti.request.getUrlandMagicKey("utilities")
                .then(options => {
                    options.method = "POST";
                    options.path += "/v1/auditLog";
                    var reqst = http.request(options, response => {
                        var data = "";
                        response.on("data", _data => data += _data.toString("utf8"));
                        response.on("end", () => {
                            //data= JSON.parse(data)
                            resolve(data);
                        });
                    });
                    reqst.end(JSON.stringify(docs))
                    reqst.on("error", function (err) {
                        reject(err);
                    });
                }).catch(err => reject(err));
        });
    }

    /**
    * 
    * @param {*result from the function compare} result 
    * @param {* the doc in DB} doc 
    * @param {*The currently modified Doc} self 
    * @param {*These are fields we need not to consider for checking the differnce between two objects} nonTrackingFields 
    */
    var buildAuditLogObjs = function (result, doc, self, nonTrackingFields) {
        var difference = [];
        //console.log("The difference key are",result.different,nonTrackingFields.toString());
        result.different = _.without(result.different, nonTrackingFields.toString());
        //console.log("THe without keys ra",result.different);
        result.different = result.different.concat(result.missing_from_first);
        result.different = result.different.concat(result.missing_from_second);
        _.map(nonTrackingFields, function (nontrackedField) {
            _.remove(result.different, function (diffrentKey) {
                var regex = new RegExp(nontrackedField, "g");
                return diffrentKey.match(regex);
            });
        });
        result.different.map(key => {
            let oldVal = _.get(doc, key); //_.get(doc, key) || null;
            let newVal = _.get(self, key); //_.get(self, key) || null;	
            oldVal = oldVal ? oldVal : oldVal === false ? oldVal : null;
            newVal = newVal ? newVal : newVal === false ? newVal : null;
            //objects are looped already.
            if ((oldVal && typeof newVal != "object") || (newVal && typeof newVal != "object")) {
                var msg = "NA";
                if (!oldVal && newVal) {
                    msg = "Key added";
                }
                if (!newVal && oldVal) {
                    msg = "Key deleted";
                }

                if ((oldVal == false && newVal) || (newVal == false && oldVal)) {
                    msg = "NA";
                }
                //merge comments 
                if (key == 'status' && options.mergeCommentsWithStatus && doc.status != self.status) {
                    _.each(options.statusCommentKeys, (_k) => {
                        if (_k && self[_k] && doc[_k] != self[_k]) {
                            msg = self[_k];
                        }
                    });
                }
                let obj = {
                    "entityType": entityType,
                    "entityId": doc._id,
                    "field": key,
                    "type": `${key} changed`,
                    "oldValue": oldVal,
                    "newValue": newVal,
                    "message": msg,
                    "createdAt": Date.now(),
                    "createdBy": self.modifiedBy || ""
                };
                difference.push(obj);
            }
        });

        result.diff = difference;
        return difference;
    }

    /**
    * This function will find the difference between to objects and gives the diffence
    * @param {*document from the DB} doc 
    * @param {*current Document} self 
    */
    var compare = function (doc, self) {
        var result = {
            diff: [],
            different: [],
            missing_from_first: [],
            missing_from_second: []
        };
        _.reduce(doc, function (result, value, key) {
            if (self.hasOwnProperty(key)) {
                if (_.isEqual(value, self[key])) {
                    return result;
                } else {
                    if (key == "_id") {
                        //dont log change if object id.
                        // TODO: verify 
                        return result;
                    } else if (typeof (doc[key]) != 'object' || typeof (self[key]) != 'object') {
                        //dead end.
                        result.different.push(key);
                        return result;
                    } else {
                        var deeper = compare(doc[key], self[key]);
                        result.different = result.different.concat(_.map(deeper.different, (sub_path) => {
                            return key + "." + sub_path;
                        }));

                        result.missing_from_second = result.missing_from_second.concat(_.map(deeper.missing_from_second, (sub_path) => {
                            return key + "." + sub_path;
                        }));

                        result.missing_from_first = result.missing_from_first.concat(_.map(deeper.missing_from_first, (sub_path) => {
                            return key + "." + sub_path;
                        }));
                        return result;
                    }
                }
            } else {
                result.missing_from_second.push(key);
                return result;
            }
        }, result);

        _.reduce(self, function (result, value, key) {
            if (doc.hasOwnProperty(key)) {
                return result;
            } else {
                result.missing_from_first.push(key);
                return result;
            }
        }, result);
        return result;
    };
};


module.exports = masterLogger;